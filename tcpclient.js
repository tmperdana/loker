var net = require('net');
// var http = require('http');
var HOST = '192.168.168.12';
var PORT = 6969;
var client = null;

var message = "";

try{
	// openConnectionData();

	function openConnectionData(){
		client  = new net.Socket();
		client.connect(PORT, HOST);
		client.on('connect',function(){
		  console.log('Client: connection established with server');

		  console.log('---------client details -----------------');
		  var address = client.address();
		  var port = address.port;
		  var family = address.family;
		  var ipaddr = address.address;
		  console.log('Client is listening at port' + port);
		  console.log('Client ip :' + ipaddr);
		  console.log('Client is IP4/IP6 : ' + family);


		  // writing data to server
		  client.write('hello from client');

		});
		client.setEncoding('utf8');

		client.on('data',function(data){
		  console.log('Message from server => ' + data, HOST);
		});

		client.on('error',function(err){
		  client = null
		  console.log('Connection lose from server:' , err);
		  // client.end();
		});
		process.stdin.setEncoding('utf-8');
		process.stdin.on('data', function (text) {
		  		text = text.replace('\n', "");
		  		text = text.replace('\r', "");
		  		// console.log(text, 'send' == text, "send")
			    // If user input 'send\n' then send all user input data to udp server.
			    if('send' == text) {
			        // If user do not input data in command line then send default message.
			        
			        console.log("Client send : " + message);
			        // Create a node Buffer object to wrap message object.
			        // message = new Buffer(message);
			        var senddata = message;
			        message = ""
			        // Send message to udp server through client socket.
			        client.write(senddata);
			    }else{
			        // Concat all user input text in message.
			        message += text;
			    }
		});
	}



	

	var connectStatus = 0;
	setInterval(function(){
	  if (client == null) {
	  	
	  	connectStatus = 0;
	  	client = null;
	  	console.log('Reconnect from server');
	  	openConnectionData();
	  }else{
	  	if (connectStatus == 0) {
	  		connectStatus = 1;
	  		console.log('Connection open now');
	  	}
	  
	  }
	  
	},5000);
}catch(error){
	console.log(error)
}


// server.listen(PORT, HOST);
