var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var bodyParser = require('body-parser');
var fs = require('fs');
var session = require('express-session');
var engine = require('ejs-locals');
var port = process.env.PORT || 8080;
var path = require('path');
var rp = require('request-promise');

var JsonDB = require('node-json-db');
var request = require('request');
const sudos = require('sudo');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.engine('ejs', engine);
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'assets'))); // assets berada di folder views
app.set('views', path.join(__dirname, '/view')); // views berada di folder views

const serverPath = "/lokerr";

var exec = require('child_process').exec
setTimeout(function (){
	exec('chromium-browser --kiosk http://localhost:8080 --incognito --disable-pinch --overscroll-history-navigation=0' , function(err) {
		if(err == null){ //process error
		  console.log("success rotate display")
			 

		}else{ console.log('Error : ' , err) }

	})
}, 1000)


// handle error
// ============= ROUTE ==============
var routes = require('./route');
// var admin = require('./route-pengguna');

// // use call router
app.use('/', routes);

app.use(function(req, res, next){
  res.status(404);
  // respond with html page
  if (req.accepts('html')) {
  	res.send(req.url);
    // res.render('404', { url: req.url });
    return;
  }
  // respond with json
                                                                                        
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');
});
replacebackup()
setTimeout(function(){
	  sendPostLog();
	  getMonitor()
	  getNTPserver(); 
	  getActivate();
	  getPaxData();
	  getDatawaktu()
	  
	  matchlog()
}, 1000);


function getNTPserver()
{
	try{
		var db = new JsonDB(__dirname+"/db/db.json", true, false);
		var config = db.getData("/");
	    var url  = "http://" + config.ip_server + serverPath +"/Transaksi/getNTP";
		var options = {
			uri: url,
			method: "GET",
			timeout: 3000, // 1 s.
			json: true // Automatically parses the JSON string in the response
		};
		rp(options)
			.then(function (repos) {
				var options = {
					cachePassword: true,
					prompt: 'raspberry'
					//spawnOptions: { /* other options for spawn */ }
				};
				if(repos.status == "success"){
					var childf = sudos(['timedatectl', "set-time", repos.data], options);
					childf.stdout.on('data', function (data) {
						// 
					});
					console.log("GET NTP SERVER "+ config.ip_server, repos.data)
				}else{
					console.log("GET ERROR NTP SERVER "+ config.ip_server )
					setTimeout(function(){
			  	  		getNTPserver();
			  		}, 500)

				}
				
			})
			.catch(function (err) {
				
				setTimeout(function(){
			  	  	getNTPserver();
			  	 }, 500)
				console.log("GET ERROR NTP SERVER "+ config.ip_server, err)
				console.log(err)
				// API call failed...
			});
	}catch(error){
		setTimeout(function(){
			  getNTPserver();
		}, 500)
		console.log("error ntp");
		console.log(error)
	}
}
function replacebackup(){
	try{
		var fileabckup = fs.readFileSync(__dirname+'/db/db.json.backup','utf8');
		fs.writeFileSync(__dirname+'/db/db.json',fileabckup,'utf8');
	}catch(error){
		console.log("backup hancur")
	}
	
}

function getMonitor(){
	var db = new JsonDB(__dirname+"/db/db.json", true, false);
	
	try {
	    var config = db.getData("/");
	    var url  = "http://" + config.ip_server + serverPath +"/Transaksi/" +"getMonitorTrs/837498UYSTG2892738728021793824ASD";
		var options = {
			uri: url,
			method: "GET",
			timeout: 2000, // 1 s.
			json: true // Automatically parses the JSON string in the response
		};
		 
		rp(options)
			.then(function (repos) {
				if (repos.status == "success") {
					var fromserver = repos["data"];
					try {
						var strserver = JSON.stringify(fromserver);
						var monitor = fs.readFileSync(__dirname+'/db/monitor.json','utf8');
						if (strserver != monitor) {
							
							console.log("update monitor")
							fs.writeFileSync(__dirname+'/db/monitor.json',strserver,'utf8');
							setTimeout(function(){
			  	  				getMonitor();
			  				}, 500)

						}else{
							setTimeout(function(){
			  	  				getMonitor();
			  				}, 500)

						}

					}catch(error){
						setTimeout(function(){
			  	  				getMonitor();
			  			}, 500)

					}
					
				}else{
					setTimeout(function(){
			  	  				getMonitor();
			  			}, 500)

				}
				
			})
			.catch(function (err) {
				
				setTimeout(function(){
			  	  	getMonitor();
			  	 }, 500)
				console.log("monitor get failed")
				// API call failed...
			});
	    
	  } catch(error) {
	  	  setTimeout(function(){
	  	  	getMonitor();
	  	  }, 500)
	  };
}

function getPaxData(){
  var db = new JsonDB(__dirname+"/db/db.json", true, false);
  var pax = new JsonDB(__dirname+"/db/pax.json", true, false);
  var trs = new JsonDB(__dirname+"/db/transaksi.json", true, false);
  var temp = new JsonDB(__dirname+"/db/transaksi_temp.json", true, false);
  try {
    var config = db.getData("/");
    var url  = "http://" + config.ip_server + serverPath +"/Transaksi/" +"getpax/"+config.lantai+"/"+config.pax;
    // console.log(url);
    var getpax = pax.getData("/");
    var strpax = JSON.stringify(getpax);
    request({
        url : url,
        method : "GET",
        json : true,
        timeout: 600,
        // body :
        async :true //
      }, function (error, response, body){
        if (error == null) {
          // console.log(body);
       //   db.push("/", {connection : "online"}, false)
          try {
		   if (body.status == "success") {
	            // ada data
	            try{
	              // console.log(getpax)
	            	var datapax = body.data[0];
	            	if (strpax != JSON.stringify(datapax)) {
	                // update data
		                pax.push("/",datapax);                
		                contNoLocker = datapax["no_start"]-0;
		                var getTrs = trs.getData('/loker');
		                for(var x in getTrs){
							
							var status = getTrs[x]["status"];
							if(status == 1 ){
						          temp.push("/"+getTrs[x]['_id'] ,getTrs[x], false);
							}
						}
						console.log("Temp Update");
						var max_loker = datapax['max_cabin'];
						var _data_loker = [];
					
						for(var n=0; n < max_loker; n++){
							var _data = {};
							_data['_id'] = "C"+(n+1);
							_data['no_kartu'] = "";
							_data['no_locker'] = (n+1);
							_data['status'] = 0;
							_data['status_locker'] = 1;
							_data['no_lockerview'] = contNoLocker;
							_data_loker.push(_data);
							contNoLocker++;
						}
						trs.push("/loker", _data_loker);
						setTimeout(function(){
						   getPaxData();
						}, 500)

              		}else{
						setTimeout(function(){
						   getPaxData();
						}, 500)
	   				}
            	}catch(e){
            		console.log("error format getPaxData");
	            	setTimeout(function(){
					   getPaxData();
					}, 500)

            	}

          }else {
            console.log( "Failed getPaxData response ON " + url);
			setTimeout(function(){
			   getPaxData();
			}, 500)

          }
		 }catch(error){
	          console.log(error.toString())
			  setTimeout(function(){
				   getPaxData();
				}, 500)

		 }
          
        }else{
		  setTimeout(function(){
			   getPaxData();
			}, 500)
        }
      });
  } catch(error) {
  	console.log(error.toString())
     setTimeout(function(){
		getPaxData();
	     }, 500)
	 };
}

function getkaryawandata(){
  var db = new JsonDB(__dirname+"/db/db.json", true, false);
  var karyawan = new JsonDB(__dirname+"/db/karyawan.json", true, false);
  try {
    var config = db.getData("/");
    //console.log(config)
    var url  = "http://" + config.ip_server + serverPath +"/Transaksi/" +"getkaryawan/";
    //console.log(url);
    var getkaryawan = karyawan.getData("/");
    var strkaryawan = JSON.stringify(getkaryawan);
    request({
        url : url,
        method : "GET",
        json : true,
        // body :
        async :true //
      }, function (error, response, body){
        if (error == null) {
          // console.log(body);
          try {
			  if (body.status == "success") {
				// ada data
				try{
				  // console.log(body.data)
				  var datakaryawan = body.data;
				  if (strkaryawan != JSON.stringify(datakaryawan)) {
					// update data
					karyawan.push("/",datakaryawan)
					console.log("KAryawan UPDATE");
				  }
				  setTimeout(function(){
					 getkaryawandata();
				  }, 500)

				  // res.send(body.data)
				}catch(e){
				  errorlog("encode", "Error data cannot be encode ON " +url)
				  setTimeout(function(){
					 getkaryawandata();
				  }, 500)
				}
			  }else {
				// errorlog("response", "Failed response ON " +url)
				setTimeout(function(){
					 getkaryawandata();
				}, 500)

				// console.log("No Connected Host");
			  }
	      }catch(error){
			setTimeout(function(){
			  getkaryawandata();
			}, 500)
			console.log(error)
	      }
        }else{
		  errorlog("http", "Server not connected ON " +url)
		  setTimeout(function(){
	         	 getkaryawandata();
		  }, 500)

        }

      });
  } catch(error) {
	setTimeout(function(){
           getkaryawandata();
	}, 500)
  }
}



function getDatawaktu(){
  var db = new JsonDB(__dirname+"/db/db.json", true, false);
  var waktu = new JsonDB(__dirname+"/db/waktu.json", true, false);
  try {
    var config = db.getData("/");
    //console.log(config)
    var url  = "http://" + config.ip_server + serverPath +"/Transaksi/" +"getwaktu/";
    //console.log(url);
    var getdatawaktu = waktu.getData("/");
    var strwaktu = JSON.stringify(getdatawaktu);
    request({
        url : url,
        method : "GET",
        json : true,
        // body :
        async :true //
      }, function (error, response, body){
        if (error == null) {
			// console.log(body)
          try {
			  if (body.status == "success") {
				// ada data
				try{
				  // console.log(body.data)
				  var datawaktu = body.data;
				  if (strwaktu != JSON.stringify(datawaktu)) {
					// update data
					waktu.push("/",datawaktu)
					console.log("Waktu UPDATE");
					
				  }
				  setTimeout(function(){
				    getDatawaktu();
     				  }, 500)

				  // res.send(body.data)
				}catch(e){
				  errorlog("encode", "Error data cannot be encode ON " +url)
				  setTimeout(function(){
				    getDatawaktu();
     				  }, 500)
				}

			  }else {
				// errorlog("response", "Failed response ON " +url)
				setTimeout(function(){
				    getDatawaktu();
     				}, 500)

				// console.log("No Connected Host");
			  }
	      }catch(error){
			console.log(error)
			setTimeout(function(){
				    getDatawaktu();
     			}, 500)
	      }
        }else{
		  setTimeout(function(){
			 getDatawaktu();
	     	  }, 500)

	    }

      });
  }catch(error) {
	setTimeout(function(){
		 getDatawaktu();
     	}, 500)

	  errorlog("software", "Software get error please update")
      //console.error(error);
  };
}



function getActivate(){
  var db = new JsonDB(__dirname+"/db/db.json", true, false);
  var trs = new JsonDB(__dirname+"/db/transaksi.json", true, false);
  var tdata = trs.getData('/loker');
  try {
    var config = db.getData("/");
    var url  = "http://" + config.ip_server + serverPath +"/Transaksi/" +"getActivateCabin/"+config.lantai+"/"+config.pax;
    //console.log(config);
	// console.log(url)
    request({
        url : url,
        method : "GET",
        json : true,
        async :true //
      }, function (error, response, body){
        if (error == null) {
         //console.log(body);
          try {
			  if (body.status == "success") {
				var result = body.data;
				for(var i in result){
					// console.log(trs)
					for(var x in tdata){
						var dt = tdata[x];
						var idlocal = dt['_id'];
						var idserver = "C"+result[i]["no_locker"];
						// console.log(idlocal , idserver)
						if(idlocal == idserver){
							var st = result[i]['status_locker'];
							console.log(" enable/disable "+ idlocal , idserver, st)
							
							var sr = {};
								sr.status_locker = result[i]['status_locker'];
							
							trs.push("/loker["+x+"]", sr, false);
							console.log("change status locker "+idserver, (st == 1) ? "Aktif" : "Tidak Aktif" )
						}
					}
				}
				setTimeout(function(){
			  	  		getActivate();
			  	}, 500)

			  }else {
				//errorlog("response", "Failed response ON " +url)
				// console.log("No Connected Host");
				setTimeout(function(){
			  	  		getActivate();
			  	}, 500)

			  }
	      }catch(error){
			console.log(error)
			setTimeout(function(){
			  getActivate();
		  	}, 500)

	      }
          
        }else{
			setTimeout(function(){
			  getActivate();
		  	}, 500)
        }

      });
  } catch(error) {
      setTimeout(function(){
		getActivate();
	  }, 500)

  };
}

function errorlog(type, msg){
	try{
		var stat = fs.statSync(__dirname + "/db/error_msg.json");
		if( (stat.size/1024) >= 90 ){
			var fsize = (stat.size/1024)  + "Kb";
			console.log("full max 100Kb current size is "+ fsize)
			fs.writeFileSync(__dirname + "/db/error_msg.json", "{}")
		}
		
		var db = new JsonDB(__dirname+"/db/error_msg.json", true, false);
		var now = new Date();
		var m = now.getMonth()+1;
		var d = now.getDate();
		var month = (m<10) ? "0"+m : m;
		var date = (d<10) ? "0"+d : d;
		var h = (now.getHours() < 10 ) ? "0"+now.getHours() : now.getHours() ;
		var m = (now.getMinutes() < 10 ) ? "0"+now.getMinutes() : now.getMinutes() ;
		var s = (now.getSeconds() < 10 ) ? "0"+now.getSeconds() : now.getSeconds() ;
		var date = now.getFullYear() + "-" + month + "-" + date;
		
		var time = h + ":" + m + ":" + s;
		var data = {};
		data["_id"] = now.getTime();
		data["type"] = type;
		data["error_msg"] = msg;
		data["date"] = date;
		data["time"] = time;
		db.push("/error[]", data);
	}catch(error){
		//console.log(error)
	}
}

function checkconnection(){
	var db = new JsonDB(__dirname+"/db/db.json", true, false);
  
  try {
    var config = db.getData("/");
    var url  = "http://" + config.ip_server + serverPath +"/Transaksi/" +"checkConnection/";
 //   console.log(url)
	var options = {
		uri: url,
		method: "GET",
		timeout: 200, // 1 s.
		json: true // Automatically parses the JSON string in the response
	};
	 
	rp(options)
		.then(function (repos) {
			db.push("/connection", "online", false)
			var data = db.getData("/connection");
			// console.log("connection = "+data);
		})
		.catch(function (err) {
			
			// console.log('User has %d repos', repos.length);
			db.push("/connection", "offline", false)
			var data = db.getData("/connection");
			// console.log("connection = "+data);
			// API call failed...
		});
    
  } catch(error) {
      console.log(error);
      //errorlog("software", "Software get error please update")
  };
}

function matchlog(){
	try {
		// console.log(1)
		var db = new JsonDB(__dirname+"/db/db.json", true, false);
		var monitordb = new JsonDB(__dirname+"/db/monitor.json", true, false);
		var trsdb = new JsonDB(__dirname+"/db/transaksi.json", true, false);
		var karyawandb = new JsonDB(__dirname+"/db/karyawan.json", true, false);
		var config = db.getData("/");

		var url  = "http://" + config.ip_server + serverPath +"/Transaksi/getcheckmonitor/"+config.lantai+"/"+config.pax;
	  	// console.log(url)
	  	var options = {
			uri: url,
			method: "GET",
			timeout: 2500, // 2.5 s.
			json: true // Automatically parses the JSON string in the response
		};
		rp(options)
		.then(function (repos) {
			try {
				if (repos.status == "success") {
					var dbserver = repos.data;
					var karyawan = karyawandb.getData('/');
					var trs = trsdb.getData("/loker")
					var pulldata = [];
					for(var i in dbserver){
						for(var x in trs){
							var dbmonit = dbserver[i];
							var trsansaksi = trs[x];
							if (dbmonit["no_locker"] == trsansaksi["no_lockerview"]) {
								if (dbmonit["log_status"] != trsansaksi["status"] ) {
									// there are somthing wrong
									var datax = dbmonit;
									// console.log(karyawan)
									if ( trsansaksi["status"] == 1) {
										for(var n in karyawan){
											
											if (karyawan[n]["no_kartu"] ==  trsansaksi["no_kartu"] ) {

												datax["id_karyawan"] = karyawan[n]["id_karyawan"];
												datax["nama_karyawan"] = karyawan[n]["nama_karyawan"];
												datax["no_kartu"] = trsansaksi["no_kartu"];

												
												break;
											}
										}
										datax["log_status"] = trsansaksi["status"];

										pulldata.push(datax)
										break;
									}else{
										datax["id_karyawan"] = "";
										datax["nama_karyawan"] = "";
										datax["no_kartu"] = "";
										datax["log_status"] = trsansaksi["status"];
										pulldata.push(datax)
										break;
									}
									
									break;
								}
							}
						}
					}
					// console.log(pulldata)
					if (pulldata.length > 0) {
						// console.log(1, "sending data")
						setTimeout(function(){
							//matchlog();
							sendDataMonitorMatch(pulldata,config.ip_server, config.lantai, config.pax )
						}, 500)
						setTimeout(function(){
							matchlog();
						}, 8000)
					}else{
						setTimeout(function(){
							matchlog();
						}, 5000)
					}
					
					
				}
				else{
					setTimeout(function(){
						matchlog();
					}, 5000)
				}
			}catch(error) {
				setTimeout(function(){
					matchlog();
				}, 5000)
			}
		})
		.catch(function (err) {
			// console.log(error);
			setTimeout(function(){
				matchlog();
			}, 5000)
		})

	} catch(error) {
	      // console.log(error);
	      setTimeout(function(){
				matchlog();
			}, 5000)
	  //errorlog("software", "Software get error please update")
	};

}

function sendDataMonitorMatch(pulldata,ip, lantai, pax ){
	var url2  = "http://" + ip + serverPath +"/Transaksi/postmonitor/"+lantai+"/"+pax;
	var myJSONObject = {'foo' : JSON.stringify(pulldata)};
	var opt1 = {
		uri: url2,
		method: "POST",
		form: myJSONObject,
		headers: {
			'content-type': 'application/x-www-form-urlencoded'  // Is set automatically
		},
		timeout: 2500, // 2.5 s.
		json: true // Automatically parses the JSON string in the response
	}
	rp(opt1)
	.then(function (repos) {
		// console.log(repos)
	})
	.catch(function (err) {
				
	})
	
			
}


function sendPostLog(){
	try {
		var db = new JsonDB(__dirname+"/db/db.json", true, false);
  		var log = new JsonDB(__dirname+"/db/translog.json", false, false);
	    var config = db.getData("/");
    	var listlog = log.getData("/datalog");
    	var loglen = Object.keys(listlog).length;
    	//console.log(Object.keys(listlog).length)
    	if(loglen > 0){
    		var url  = "http://" + config.ip_server + serverPath +"/Transaksi/sendPostLog/"+config.lantai+"/"+config.pax;
			var myJSONObject = {'foo' : JSON.stringify(listlog)};
			var options = {
				uri: url,
				method: "POST",
				form: myJSONObject,
				headers: {
					'content-type': 'application/x-www-form-urlencoded'  // Is set automatically
				},
				timeout: 2500, // 2.5 s.
				json: true // Automatically parses the JSON string in the response
			};
			rp(options)
			.then(function (repos) {
				// console.log(repos)
				try{
					// console.log(repos.status, loglen)
					
					var ss = 0;
					
					for(var x in listlog){
						ss++;
						log.delete('/datalog/'+x);
					}

			     	if ( loglen == ss)
					{
						 log.save();
						 console.log("SENDLOG GET SUCCESS SEND")
						 setTimeout(function(){
							sendPostLog();
						 }, 500)
					 }else{
						 console.log("SENDLOG " ,loglen , ss)
						log.save();
						setTimeout(function(){
							sendPostLog();
						 }, 500)
					 }
					 
			     	
					
					
				}catch(e){
					
					console.log("SENDLOG GET FAILED ERROR", new Date())
					setTimeout(function(){
						sendPostLog();
					}, 500)
				}				
			})
			.catch(function (err) {
				//console.log(err);
				fs.writeFileSync(__dirname+'/error.txt',JSON.stringify(err),'utf8');
			  	setTimeout(function(){
					sendPostLog();
				}, 500)
			});
    	}else{
    		// NO DATA
    		
    		setTimeout(function(){
				 sendPostLog();
			}, 500)
    	}

	    
	} catch(error) {
		fs.writeFileSync(__dirname+'/error.txt',JSON.stringify(err),'utf8');
		console.log("SENDLOG GET FAILED ");
	  	setTimeout(function(){
			sendPostLog();
		}, 500)
	};
}


http.listen(port, function() {
    console.log('listening on *:' + port);
    console.log('server running ' + 'now ' + Date.now());
});
