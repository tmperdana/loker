var express = require('express');
var app = require('express')();
var http = require('http');

const WebSocket = require('ws');

const server = http.createServer(app);
process.stdin.setEncoding('utf-8');

var message = ""
var clients={};
clients["example"] = {};
var wss = new WebSocket.Server({server:server, path:"/example"});
// console.log(wss.clients)

wss.on('connection', function(ws, req){
	var ip = req.connection.remoteAddress;
	clients["example"][ip] = ws;
	// console.log(ws._socket.address());
	ws.on('message', function(message) {
   		console.log('received: %s', message);
	});


});

process.stdin.on('data', function (text) {
	  		text = text.replace('\n', "");
	  		text = text.replace('\r', "");
	  		// console.log(text, 'send' == text, "send")
		    // If user input 'send\n' then send all user input data to udp server.
		    if('send' == text) {
		        // If user do not input data in command line then send default message.
		        if (message == null || message.length == 0) {
		            // message = "Hello tcp client.";

		        }
		        // console.log("Server send : " + message);
		        // Create a node Buffer object to wrap message object.
		        // message = new Buffer(message);
		        var senddata = message;
		        message = "";
		        // Send message to udp server through client socket.
		        // ws.send(senddata);
		        sendBlast("example", senddata)
		        
		    }else{
		        // Concat all user input text in message.
		        message += text;
		    }
	});

function sendBlast(route, msg, ip = ""){
	if (ip == "") {
		var data = clients[route];
		for(var x in data){
			var client = data[x];
			if (client.readyState === WebSocket.OPEN) {
				console.log("sending message to "+x, " "+msg);
				var json = {
		        	"msg": msg,
		        	"timeint" : Date.now(),
		        	"length" : msg.length
		        }
				// client.send(msg+" "+ new Date());

				client.send(JSON.stringify(json));
			}
		}
	}else{
		var client = clients[route][ip];
		if (client != null) {
			// ada

			if (client.readyState === WebSocket.OPEN) {
				client.send(data);
			}
		}
		// spesific
	}
}
	

server.listen(3000, function () {
   console.log('Example app listening on port 3000!')
})