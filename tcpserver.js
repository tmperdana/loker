var net = require('net');

var HOST = '192.168.168.12';
var PORT = 6969;

var server = net.createServer();

var clientstore = {};
server.listen(PORT, HOST);

message = ""
// console.log('Server listening on ' + server.address().address +':'+ server.address().port);
server.on('connection', function(sock) {
  var cl = {};
  // console.log(sock)
  var ip = sock.remoteAddress;
  cl["ip"] = sock.remoteAddress;
  cl["port"] = sock.remotePort;
  clientstore[ip] = cl;
  console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
  sock.on('error', function (err) {
	    console.log('Connection %s error: %s', sock.remoteAddress, err.message);
  });
  sock.on('data', function (data) {
	    console.log('Message from client => '+data, ip);
  });

  // setTimeout(function(){
		// sock.write('Echo server\r\n');
  // }, 3000)
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', function (text) {
  		text = text.replace('\n', "");
  		text = text.replace('\r', "");
  		// console.log(text, 'send' == text, "send")
	    // If user input 'send\n' then send all user input data to udp server.
	    if('send' == text) {
	        // If user do not input data in command line then send default message.
	        if (message == null || message.length == 0) {
	            // message = "Hello tcp client.";

	        }
	        // console.log("Server send : " + message);
	        // Create a node Buffer object to wrap message object.
	        // message = new Buffer(message);
	        var senddata = message;
	        message = "";
	        // Send message to udp server through client socket.
	        sock.write(senddata);
	        
	    }else{
	        // Concat all user input text in message.
	        message += text;
	    }
  });
  // other stuff is the same from here
});



  // Add a 'close' event handler to this instance of socket
server.on('close', function(data) {
   console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
});
