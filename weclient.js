const WebSocket = require('ws');
 
const ws = new WebSocket('ws://localhost:3000/example');
var message = "";
process.stdin.setEncoding('utf-8');
ws.on('open', function open() {
  // console.log("open")
  // ws.send('something');
});
process.stdin.on('data', function (text) {
  		text = text.replace('\n', "");
  		text = text.replace('\r', "");
  		// console.log(text, 'send' == text, "send")
	    // If user input 'send\n' then send all user input data to udp server.
	    if('send' == text) {
	        // If user do not input data in command line then send default message.
	        if (message == null || message.length == 0) {
	            // message = "Hello tcp client.";

	        }
	        // console.log("Server send : " + message);
	        // Create a node Buffer object to wrap message object.
	        // message = new Buffer(message);
	        var senddata = message;
	        message = "";
	        var json = {
	        	"msg": senddata,
	        	"timeint" : Date.now(),
	        	"length" : senddata.length
	        }
	        // Send message to udp server through client socket.
	        ws.send(JSON.stringify(json));
	        
	    }else{
	        // Concat all user input text in message.
	        message += text;
	    }
});

ws.on('message', function(data) {
  console.log(data);
  var com = JSON.parse(data);
  console.log((Date.now()-com.timeint))
  console.log(com.msg, "time: "+ (Date.now()-com.timeint), com.length)
});